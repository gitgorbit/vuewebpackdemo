import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/List'
import Edit from '@/components/Edit'
import Add from '@/components/Add'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'List',
      component: List
    },
    {
      path: '/edit/:id',
      name: 'Edit',
      component: Edit,
      props: true
    },
    {
      path: '/add',
      name: 'Add',
      component: Add,
      props: true
    }
  ]
})
