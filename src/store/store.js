import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

var apiUrl = 'https://zoeyport.dev.crossvallia.ba'
var sessionKey = 'zoeyport-key'

export const store = new Vuex.Store({
    state: {
        people: [],
        loading: true,
        cities: {
            options: [
                { text: 'Zoeyport', value: 'Zoeyport' },
                { text: 'Royton', value: 'Royton' },
                { text: 'West Brisaburgh', value: 'West Brisaburgh' },
                { text: 'West Elyssamouth', value: 'West Elyssamouth' },
                { text: 'Letaview', value: 'Letaview' },
                { text: 'Cleoland', value: 'Cleoland' },
                { text: 'Theresaland', value: 'Theresaland' }
            ]
        },
        occupations: {
            options: [
                { text: 'Strategist', value: 'strategist' },
                { text: 'Coordinator', value: 'coordinator' },
                { text: 'Engineer', value: 'engineer' },
                { text: 'Supervisor', value: 'supervisor' },
                { text: 'Planner', value: 'planner' },
                { text: 'Designer', value: 'designer' },
                { text: 'Producer', value: 'producer' },
                { text: 'Specialist', value: 'specialist' }
            ]
        }
    },
    actions: {
        loadData ({ commit }) {
            if (sessionStorage.getItem(sessionKey) === null || this.state.people.length === 0) {
                axios.get(apiUrl)
                    .then(response => {
                        response.data.map((currentValue, index) => {
                            currentValue.id = index + 1
                        })
                        commit('loadPeople', response.data)
                        commit('changeLoadingState', false)
                        sessionStorage.setItem(sessionKey, JSON.stringify(response.data))
                    })
                    .catch(error => console.log(error))
            }
             JSON.parse(sessionStorage.getItem(sessionKey))
        },
        getById ({ commit }, id) {
            // console.log(id)
            commit('getById', id)
        },
        loadCities ({ commit }) {
            commit('loadCities')
        },
        loadOccupations ({ commit }) {
            commit('loadOccupations')
        }
    },
    mutations: {
        loadPeople (state, people) {
            state.people = people
        },
        updatePeople (state, person) {
            var searched = state.people.find(val => {
                return val.id === person.id
            })
            searched = person
        },
        addPeople (state, person) {
            var personWithMaxId = state.people.reduce((prev, current) => {
                return (prev.id > current.id) ? prev : current
            })
            person.id = personWithMaxId.id++
            state.people.push(person)
        },
        changeLoadingState (state, loading) {
            state.loading = loading
        },
        getById (state, id) {
            var founded = state.people.find(val => {
                return val.id === id
            })
            return founded
        },
        loadCities (state) {
            return state.cities
        },
        loadOccupations (state) {
            return state.occupations
        }
    }
})
